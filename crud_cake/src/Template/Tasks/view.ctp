<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Task $task
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Editar Tarefas'), ['action' => 'edit', $task->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Deletar Tarefas'), ['action' => 'delete', $task->id], ['confirm' => __('Você tem certeza que deseja excluir # {0}?', $task->id)]) ?> </li>
        <li><?= $this->Html->link(__('Listar Tarefas'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Nova Tarefa'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="tasks view large-9 medium-8 columns content">
    <h3><?= h($task->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Nome') ?></th>
            <td><?= h($task->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Conteúdo') ?></th>
            <td><?= h($task->content) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($task->id) ?></td>
        </tr>
    </table>
</div>
