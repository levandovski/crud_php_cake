<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Novo Usuário'), ['controller' => 'Users','action' => 'add?a=1']) ?></li>
    </ul>
</nav>
<div class="users form large-9 medium-8 columns content">
     <?= $this->Form->create() ?>
     <fieldset>
        <legend><?= __('Login') ?></legend>
        <?php
            echo $this->Form->control('username');
            echo $this->Form->control('password');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Logar')) ?>
    <?= $this->Form->end() ?>
</div>
